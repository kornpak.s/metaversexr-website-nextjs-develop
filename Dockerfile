FROM node:16.15.0-alpine

WORKDIR /app
COPY . .

RUN npm install


CMD ["npm", "run", "production"]